This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Smart Executor API

## [v3.1.0]

- updated gcube-bom

## [v3.0.0]

- Switched smart-executor JSON management to gcube-jackson [#19647]
- Plugins must provide a property file with name <PluginName>.properties [#21596]  


## [v2.0.0] 

- Changed some exception to enable RESTful Smart Executor Service to better provide HTTP status error


## [v1.7.0] [r4.10.0] - 2018-02-15

- Added support to have REST interface in Smart Executor [#5109]
- Changed pom.xml to use new make-servicearchive directive [#10169]


## [v1.6.0] [r4.7.0] - 2017-10-09

- Added jackson support for json marshalling/unmarshalling


## [v1.5.0] [r4.3.0] - 2017-03-16

- Provided access to UUID and iteration number for a plugin [#6733]
- Added support to implements Reminiscence for a Scheduled Task [#772]


## [v1.4.0] [r4.1.0] - 2016-11-07

- SmartExecutor has been migrated to Authorization 2.0 [#4944] [#2112]
- Provided to plugins the possibility to specify progress percentage [#440]
- Provided to plugins the possibility to define a custom notifier [#5089]
- Using newly created gcube-bom [#2363]


## [v1.3.0] [r3.10.0] - 2016-02-08

- Added Unscheduling feature for repetitive task [#521]


## [v1.2.0] [r3.9.0] - 2015-12-09

- Added Recurrent and Scheduled Task support [#111]


## [v1.1.0] - 2015-08-31

- Added init() function on plugins


## [v1.0.0] - 2015-02-05

- First Release

