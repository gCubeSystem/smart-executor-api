package org.gcube.vremanagement.executor.api.rest;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class RestConstants {
	
	public static final String PLUGINS_PATH_PART = "plugins";
	
	public static final String EXECUTIONS_PATH_PART = "executions";
	
	public static final String ITERATION_PARAM = "iteration";
	
	public static final String UNSCHEDULE_PARAM = "unschedule";
	
	public static final String ORPHAN_PATH_PARAM = "ORPHAN";
	
}
