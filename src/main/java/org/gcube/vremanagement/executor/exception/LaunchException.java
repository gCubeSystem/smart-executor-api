/**
 * 
 */
package org.gcube.vremanagement.executor.exception;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class LaunchException extends ExecutorException {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -4805436085852176907L;
	
	private static final String DEFAULT_MESSAGE = "Error trying to launch the plugin execution";
	
	public LaunchException() {
		super(DEFAULT_MESSAGE);
	}
	
	public LaunchException(Throwable cause) {
		this(DEFAULT_MESSAGE, cause);
	}
	
	public LaunchException(String message) {
		super(message);
	}
	
	public LaunchException(String message, Throwable cause){
		super(message, cause);
	}
	
}
