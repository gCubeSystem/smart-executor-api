/**
 * 
 */
package org.gcube.vremanagement.executor.exception;

import java.util.UUID;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class PluginInstanceNotFoundException extends ExecutorException {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -7609491801703267843L;

	private static final String DEFAULT_MESSAGE = "No plugin instance with UUID %s found";
	
	public PluginInstanceNotFoundException(UUID uuid) {
		super(String.format(DEFAULT_MESSAGE,uuid.toString()));
	}
	
	public PluginInstanceNotFoundException(UUID uuid, Throwable cause) {
		this(String.format(DEFAULT_MESSAGE,uuid.toString()), cause);
	}
	
	public PluginInstanceNotFoundException(String message) {
		super(message);
	}
	
	public PluginInstanceNotFoundException(String message, Throwable cause){
		super(message, cause);
	}
	
}
