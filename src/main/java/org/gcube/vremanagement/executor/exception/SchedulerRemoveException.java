/**
 * 
 */
package org.gcube.vremanagement.executor.exception;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class SchedulerRemoveException extends ExecutorException {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -7609491801703267843L;

	private static final String DEFAULT_MESSAGE = "Unable to delete SmartExecutor Scheduled Task";
	
	public SchedulerRemoveException() {
		super(DEFAULT_MESSAGE);
	}
	
	public SchedulerRemoveException(Throwable cause) {
		this(DEFAULT_MESSAGE, cause);
	}
	
	public SchedulerRemoveException(String message) {
		super(message);
	}
	
	public SchedulerRemoveException(String message, Throwable cause){
		super(message, cause);
	}
	
}
