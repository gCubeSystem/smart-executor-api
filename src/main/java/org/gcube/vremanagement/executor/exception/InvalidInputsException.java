/**
 * 
 */
package org.gcube.vremanagement.executor.exception;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class InvalidInputsException extends ExecutorException {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 7578814354528161119L;

	private static final String DEFAULT_MESSAGE = "Inputs cannot be null. Use an Empty Map instead.";
	
	public InvalidInputsException() {
		super(DEFAULT_MESSAGE);
	}
	
	public InvalidInputsException(Throwable cause) {
		this(DEFAULT_MESSAGE, cause);
	}
	
	public InvalidInputsException(String message) {
		super(message);
	}
	
	public InvalidInputsException(String message, Throwable cause){
		super(message, cause);
	}
	
}
