/**
 * 
 */
package org.gcube.vremanagement.executor.exception;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.gcube.vremanagement.executor.json.SEMapper;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = SEMapper.CLASS_PROPERTY)
public class ExecutorException extends Exception {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 150353533672078736L;

	private static final String DEFAULT_MESSAGE = "Executor Exception";
	
	public ExecutorException() {
		super(DEFAULT_MESSAGE);
	}
	
	public ExecutorException(Throwable cause) {
		this(DEFAULT_MESSAGE, cause);
	}
	
	public ExecutorException(String message) {
		super(message);
	}
	
	public ExecutorException(String message, Throwable cause){
		super(message, cause);
	}
	
}
