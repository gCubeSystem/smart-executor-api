/**
 * 
 */
package org.gcube.vremanagement.executor.exception;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class InputsNullException extends ExecutorException {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 7578814354528161119L;

	private static final String DEFAULT_MESSAGE = "Inputs cannot be null. Use an Empty Map instead.";
	
	
	public InputsNullException() {
		super(DEFAULT_MESSAGE);
	}
	
	public InputsNullException(Throwable cause) {
		this(DEFAULT_MESSAGE, cause);
	}
	
	public InputsNullException(String message) {
		super(message);
	}
	
	public InputsNullException(String message, Throwable cause){
		super(message, cause);
	}

}
