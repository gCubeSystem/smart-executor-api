/**
 * 
 */
package org.gcube.vremanagement.executor.exception;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class PluginNotFoundException extends ExecutorException {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 6591994140867585229L;

	private static final String DEFAULT_MESSAGE = "The requested plugin does not exist on container";
	
	public PluginNotFoundException() {
		super(DEFAULT_MESSAGE);
	}
	
	public PluginNotFoundException(Throwable cause) {
		this(DEFAULT_MESSAGE, cause);
	}
	
	public PluginNotFoundException(String message) {
		super(message);
	}
	
	public PluginNotFoundException(String message, Throwable cause){
		super(message, cause);
	}
	
}
