/**
 * 
 */
package org.gcube.vremanagement.executor.exception;

import java.util.UUID;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class UnableToInterruptTaskException extends ExecutorException {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -7609491801703267843L;

	private static final String DEFAULT_MESSAGE = "Unable to interrupt SmartExecutor Task";
	
	public UnableToInterruptTaskException() {
		super(DEFAULT_MESSAGE);
	}
	
	public UnableToInterruptTaskException(Throwable cause) {
		this(DEFAULT_MESSAGE, cause);
	}
	
	public UnableToInterruptTaskException(String message) {
		super(message);
	}
	
	public UnableToInterruptTaskException(String message, Throwable cause){
		super(message, cause);
	}
	
	public UnableToInterruptTaskException(UUID taskUUID, Throwable cause) {
		this(DEFAULT_MESSAGE + " with UUID " + taskUUID.toString(), cause);
	}
}
