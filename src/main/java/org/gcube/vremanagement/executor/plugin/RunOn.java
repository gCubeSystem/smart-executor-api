/**
 * 
 */
package org.gcube.vremanagement.executor.plugin;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonProperty;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.vremanagement.executor.json.SEMapper;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property=SEMapper.CLASS_PROPERTY)
@JsonTypeName(value="RunOn")
public class RunOn {
	
	@JsonProperty
	protected Ref hostingNode;
	
	@JsonProperty
	protected Ref eService;
	
	public RunOn(){
		
	}
	
	public RunOn(Ref hostingNode, Ref eService){
		this.hostingNode = hostingNode;
		this.eService = hostingNode;
	}
	
	/**
	 * @return the hostingNodeID
	 */
	public Ref getHostingNode() {
		return hostingNode;
	}

	/**
	 * @param hostingNode the hostingNode to set
	 */
	public void setHostingNode(Ref hostingNode) {
		this.hostingNode = hostingNode;
	}

	/**
	 * @return the eServiceID
	 */
	@JsonIgnore
	public Ref getEService() {
		return eService;
	}

	/**
	 * @param eService the EService to set
	 */
	@JsonIgnore
	public void setEService(Ref eService) {
		this.eService = eService;
	}

}
