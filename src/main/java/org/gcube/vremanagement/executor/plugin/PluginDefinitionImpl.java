/**
 * 
 */
package org.gcube.vremanagement.executor.plugin;

import java.util.Map;

import org.gcube.vremanagement.executor.json.SEMapper;

/**
 * @author Luca Frosini (ISTI - CNR)
 * Used for JSON serialization only
 */
class PluginDefinitionImpl implements PluginDefinition {

	protected String group;
	protected String name;
	protected String version;
	protected String description;
	protected Map<String, String> supportedCapabilities;
	
	protected PluginDefinitionImpl() {}
	
	@Override
	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}
	
	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	@Override
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public Map<String, String> getSupportedCapabilities() {
		return supportedCapabilities;
	}

	public void setSupportedCapabilities(Map<String, String> supportedCapabilities) {
		this.supportedCapabilities = supportedCapabilities;
	}

	@Override
	public String toString() {
		try {
			return SEMapper.getInstance().marshal(this);
		} catch(Exception e) {
			return "PluginDefinitionImpl [group=" + group + ", name=" + name + ", version=" + version + ", description="
				+ description + "]";
		}
	}

	
	
}
