/**
 * 
 */
package org.gcube.vremanagement.executor.plugin;

import java.util.Map;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.vremanagement.executor.json.SEMapper;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property=SEMapper.CLASS_PROPERTY)
@JsonDeserialize(as=PluginDefinitionImpl.class)
@JsonTypeName(value="PluginDefinition")
public interface PluginDefinition {

	/**
	 * This method is used by executor to retrieve the Plugin group
	 * It will be published on the IS instance created by the executor
	 * @return the plugin group.
	 */
	public String getGroup();
	
	/**
	 * This method is used by executor to retrieve the Plugin name
	 * It will be published on the IS instance created by the executor
	 * @return the plugin name.
	 */
	public String getName();
	
	/**
	 * This method is used by executor to retrieve the plugin version.
	 * It will be published on the IS instance created by the executor
	 * @return the plugin version.
	 */
	public String getVersion();
	
	/**
	 * This method is used by executor to retrieve the Plugin human oriented description.
	 * It will be published on the IS instance created by the executor
	 * @return the plugin description.
	 */
	public String getDescription();
	
	/**
	 * This method is used by the Executor to get a key-value {@link Map}
	 * to be published on IS , so a client which want to 
	 * launch a Plugin only under certain condition can query the IS 
	 * to obtain its own filtered list.
	 * @return the {@link Map} with the supported capabilities
	 */
	public Map<String,String> getSupportedCapabilities();

}
