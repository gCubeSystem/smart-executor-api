/**
 * 
 */
package org.gcube.vremanagement.executor.plugin;

import java.util.UUID;

import org.gcube.com.fasterxml.jackson.annotation.JsonGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.gcube.vremanagement.executor.exception.InvalidPluginStateEvolutionException;
import org.gcube.vremanagement.executor.json.SEMapper;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property=SEMapper.CLASS_PROPERTY)
public class PluginStateEvolution {

	protected UUID uuid;
	
	protected int iteration;
	
	protected long timestamp;
	
	protected PluginDefinition pluginDefinition;
	
	protected PluginState pluginState;
	
	protected int percentage;
	
	protected RunOn runOn;
	
	public PluginStateEvolution(){
		
	}
	
	/**
	 * 
	 * @param uuid the UUID which identify the current execution
	 * @param timestamp the time of the new {@link PluginState}
	 * @param pluginDefinition the pluginDeclaration
	 * @param pluginState the {@link PluginState} value
	 * @throws Exception if fails
	 */
	public PluginStateEvolution(UUID uuid, int iteration, long timestamp,
			PluginDefinition pluginDefinition, 
			PluginState pluginState, Integer percentage) throws InvalidPluginStateEvolutionException {
		this.uuid = uuid;
		this.iteration = iteration;
		this.timestamp = timestamp;
		this.pluginDefinition = pluginDefinition;
		this.pluginState = pluginState;
		switch (pluginState) {
			case CREATED:
				this.percentage = 0;
				break;
	
			default:
				if(percentage<0 || percentage>100){
					throw new InvalidPluginStateEvolutionException("Percentage must be beetween 0 and 100");
				}
				this.percentage = percentage;
				break;
		}
		
	}

	/**
	 * @return the uuid
	 */
	public UUID getUUID() {
		return uuid;
	}

	/**
	 * @return the iteration
	 */
	public int getIteration() {
		return iteration;
	}

	/**
	 * @return the timestamp
	 */
	public long getTimestamp() {
		return timestamp;
	}

	/**
	 * @return the pluginDeclaration
	 */
	@JsonGetter
	public PluginDefinition getPluginDefinition() {
		return pluginDefinition;
	}

	/**
	 * @return the pluginState
	 */
	public PluginState getPluginState() {
		return pluginState;
	}
	
	/**
	 * @return the percentage of execution
	 */
	public Integer getPercentage() {
		return this.percentage;
	}

	/**
	 * @return the runOn
	 */
	protected RunOn getRunOn() {
		return runOn;
	}

	/**
	 * @param runOn the runOn to set
	 */
	protected void setRunOn(RunOn runOn) {
		this.runOn = runOn;
	}
	
	
	
	@Override
	public String toString(){
		return String.format("{"
					+ "uuid:%s,"
					+ "iteration:%d,"
					+ "timestamp:%d,"
					+ "pluginDefinition:%s,"
					+ "pluginState:%s,"
					+ "percentage:%d"
				+ "}",
				uuid, 
				iteration, 
				timestamp, 
				pluginDefinition, 
				pluginState, 
				percentage);
	}

}
