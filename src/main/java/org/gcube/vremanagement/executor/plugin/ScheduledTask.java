/**
 * 
 */
package org.gcube.vremanagement.executor.plugin;

import java.util.UUID;

import org.gcube.com.fasterxml.jackson.annotation.JsonProperty;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.gcube.vremanagement.executor.api.types.LaunchParameter;
import org.gcube.vremanagement.executor.json.SEMapper;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = SEMapper.CLASS_PROPERTY)
public class ScheduledTask {
	
	public static final String LAUNCH_PARAMETER = "launchParameter";
	
	protected UUID uuid;
	
	@JsonProperty(value = LAUNCH_PARAMETER)
	protected LaunchParameter launchParameter;
	
	protected RunOn runOn;
	
	public ScheduledTask() {
	}
	
	public ScheduledTask(UUID uuid, RunOn runOn, LaunchParameter launchParameter) {
		this.uuid = uuid;
		this.runOn = runOn;
		this.launchParameter = launchParameter;
	}
	
	/**
	 * @return the uuid
	 */
	public UUID getUUID() {
		return uuid;
	}
	
	/**
	 * @return the runOn
	 */
	public RunOn getRunOn() {
		return runOn;
	}
	
	/**
	 * @return the launchParameter
	 */
	public LaunchParameter getLaunchParameter() {
		return launchParameter;
	}
	
	@Override
	public String toString() {
		try {
			return SEMapper.getInstance().marshal(this);
		} catch(Exception e) {
			return "ScheduledTask [uuid=" + uuid + ", launchParameter=" + launchParameter + ", runOn=" + runOn + "]";
		}
	}
	
}
