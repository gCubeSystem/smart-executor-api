/**
 * 
 */
package org.gcube.vremanagement.executor.plugin;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public interface PercentageSetter {

	public void setPercentageEvolution(Integer integer);
	
}
