package org.gcube.vremanagement.executor.plugin;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represent the contract for a plugin runnable by the executor.
 * @author Luca Frosini (ISTI - CNR)
 * This class is deprecated use {@link ExecutorPlugin} instead 
 */
public abstract class Plugin implements PluginDefinition {
	
	private static Logger logger = LoggerFactory.getLogger(Plugin.class);
	

	public static final String PLUGIN_PROPERTIES_SUFFIX_FILENAME = ".properties";
	
	public static final String GROUP_PROPERTY = "groupId";
	public static final String NAME_PROPERTY = "artifactId";
	public static final String VERSION_PROPERTY = "version";
	public static final String DESCRIPTION_PROPERTY = "description";
	
	protected Properties properties;
	
	protected UUID uuid;
	protected int iterationNumber;
	
	protected PercentageSetter percentageSetter;
	
	public Plugin(){
		logger.debug(String.format("Initializing %s", this.getClass().getSimpleName()));
		
		this.percentageSetter = new PercentageSetter() {
			
			@SuppressWarnings("unused")
			private int percentage = 0;
			
			@Override
			public void setPercentageEvolution(Integer integer) {
				this.percentage = integer;
			}
			
		};
		
		properties = new Properties();
		
		try {
			String filename =  this.getClass().getSimpleName() + PLUGIN_PROPERTIES_SUFFIX_FILENAME;
			InputStream input = getClass().getClassLoader().getResourceAsStream("META-INF" + File.separator + filename);
			properties.load(input);
		} catch(IOException e) {
			throw new RuntimeException(e);
		}
		
		logger.debug(String.format("%s initialized", this.getClass().getSimpleName()));
	}
	
	@Override
	public String getGroup() {
		return properties.getProperty(GROUP_PROPERTY);
	}
	
	@Override
	public String getName() {
		return properties.getProperty(NAME_PROPERTY);
	}
	
	@Override
	public String getVersion() {
		return properties.getProperty(VERSION_PROPERTY);
	}
	
	@Override
	public String getDescription() {
		return properties.getProperty(DESCRIPTION_PROPERTY);
	}

	@Override
	public Map<String,String> getSupportedCapabilities() {
		return null;
	}
	
	/**
	 * @param percentageSetter the percentageSetter to set
	 */
	public void setPercentageSetter(PercentageSetter percentageSetter) {
		this.percentageSetter = percentageSetter;
	}
	
	protected void setPercentageEvolution(Integer integer){
		//if(this.percentageSetter!=null){
			this.percentageSetter.setPercentageEvolution(integer);
		//}
	}
	
	/**
	 * @return the uuid
	 */
	public UUID getUUID() {
		return uuid;
	}

	/**
	 * @param uuid the uuid to set
	 */
	public void setUUID(UUID uuid) {
		this.uuid = uuid;
	}

	/**
	 * @return the Iteration Number
	 */
	public int getIterationNumber() {
		return iterationNumber;
	}

	/**
	 * @param iterationNumber the iterationNumner to set
	 */
	public void setIterationNumber(int iterationNumber) {
		this.iterationNumber = iterationNumber;
	}

	/**
	 * Launch the plugin with the provided input.
	 * @param inputs
	 * @throws Exception if the launch fails
	 */
	public abstract void launch(Map<String,Object> inputs) throws Exception;
	
	/**
	 * This function is used to correctly stop the plugin 
	 * @throws Exception if the launch fails
	 */
	protected abstract void onStop() throws Exception;
	
	/**
	 * Invoke onStop() function to allow the plugin to safely stop the execution
	 * @throws Exception
	 */
	public void stop() throws Exception {
		onStop();
	}
	
}
