/**
 * 
 */
package org.gcube.vremanagement.executor.json;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.gcube.com.fasterxml.jackson.core.JsonGenerationException;
import org.gcube.com.fasterxml.jackson.core.JsonParseException;
import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.com.fasterxml.jackson.databind.DeserializationFeature;
import org.gcube.com.fasterxml.jackson.databind.JavaType;
import org.gcube.com.fasterxml.jackson.databind.JsonMappingException;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.vremanagement.executor.api.types.LaunchParameter;
import org.gcube.vremanagement.executor.api.types.Scheduling;
import org.gcube.vremanagement.executor.plugin.PluginDefinition;
import org.gcube.vremanagement.executor.plugin.PluginStateEvolution;
import org.gcube.vremanagement.executor.plugin.Ref;
import org.gcube.vremanagement.executor.plugin.RunOn;
import org.gcube.vremanagement.executor.plugin.ScheduledTask;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class SEMapper {
	
	protected final ObjectMapper mapper;
	
	public static final String CLASS_PROPERTY = "@class";
	
	protected static SEMapper instance;
	
	static {
		instance = new SEMapper();
	}
	
	public static SEMapper getInstance() {
		return instance;
	}
	
	SEMapper(){
		
		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
		mapper.registerSubtypes(RunOn.class);
		mapper.registerSubtypes(Ref.class);
		
		mapper.registerSubtypes(PluginDefinition.class);
		mapper.registerSubtypes(PluginStateEvolution.class);

		mapper.registerSubtypes(LaunchParameter.class);
		mapper.registerSubtypes(Scheduling.class);
		
		mapper.registerSubtypes(ScheduledTask.class);
		
		
	}
	
	public void registerSubtypes(Class<?> classes) {
		mapper.registerSubtypes(classes);
	}
	
	/**
	 * Write the serialization of a given resource to a given
	 * {@link OutputStream} .
	 * 
	 * @param object the resource
	 * @param stream the stream in input
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	public <T extends OutputStream, O extends Object> T marshal(O object, T stream)
			throws JsonGenerationException, JsonMappingException, IOException {
		mapper.writeValue(stream, object);
		return stream;
	}
	
	/**
	 * Write the serialization of a given resource to a given {@link Writer} .
	 * @param object the resource
	 * @param writer the writer in input
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	public <T extends Writer, O extends Object> T marshal(O object, T writer)
			throws JsonGenerationException, JsonMappingException, IOException {
		mapper.writeValue(writer, object);
		return writer;
	}
	
	/**
	 * Return the String serialization of a given object
	 * @param object the object to marshal
	 * @return the String serialization of a given resource
	 * @throws JsonProcessingException
	 */
	public <O extends Object> String marshal(O object) throws JsonProcessingException {
		return mapper.writeValueAsString(object);
	}
	
	/**
	 * Return the String serialization of a given list
	 * @param list the list to marshal
	 * @return the String serialization of a given list
	 * @throws JsonProcessingException
	 */
	public <O extends Object> String marshal(Class<O> clz, List<O> list) throws JsonProcessingException {
		JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, clz);
		return mapper.writerFor(type).writeValueAsString(list);
	}
	
	/**
	 * Return the String serialization of a given array
	 * @param array the array to marshal
	 * @return the String serialization of a given array
	 * @throws JsonProcessingException
	 */
	public <O extends Object> String marshal(O[] array) throws JsonProcessingException {
		return mapper.writeValueAsString(array);
	}
	
	/**
	 * Creates a resource of given class from its serialization in a given
	 * {@link Reader}.
	 * @param clz the class of the resource
	 * @param reader the reader
	 * @return the resource
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public <O extends Object> O unmarshal(Class<O> clz, Reader reader)
			throws JsonParseException, JsonMappingException, IOException {
		return mapper.readValue(reader, clz);
	}
	
	/**
	 * Creates a resource of given class from its serialization in a given
	 * {@link InputStream}.
	 * @param clz the class of the resource
	 * @param stream the stream
	 * @return the resource
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public <O extends Object> O unmarshal(Class<O> clz, InputStream stream)
			throws JsonParseException, JsonMappingException, IOException {
		return mapper.readValue(stream, clz);
	}
	
	/**
	 * Creates a resource of given class from its serialization in a given String
	 * @param clz the class of the resource
	 * @param string
	 * @return the resource
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public <O extends Object> O unmarshal(Class<O> clz, String string)
			throws JsonParseException, JsonMappingException, IOException {
		return mapper.readValue(string, clz);
	}
	
	public <O extends Object> List<O> unmarshalList(Class<O> clz, String string)
			throws JsonParseException, JsonMappingException, IOException {
		JavaType type = mapper.getTypeFactory().constructCollectionType(ArrayList.class, clz);
		return mapper.readValue(string, type);
	}
	
	public <O extends Object> List<O> unmarshalList(String string)
			throws JsonParseException, JsonMappingException, IOException {
		JavaType type = mapper.getTypeFactory().constructCollectionType(ArrayList.class, Object.class);
		return mapper.readValue(string, type);
	}
	
}
