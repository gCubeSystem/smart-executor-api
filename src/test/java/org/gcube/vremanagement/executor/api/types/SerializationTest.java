/**
 * 
 */
package org.gcube.vremanagement.executor.api.types;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.core.JsonParseException;
import org.gcube.com.fasterxml.jackson.databind.JsonMappingException;
import org.gcube.vremanagement.executor.exception.InvalidPluginStateEvolutionException;
import org.gcube.vremanagement.executor.json.SEMapper;
import org.gcube.vremanagement.executor.plugin.PluginDefinition;
import org.gcube.vremanagement.executor.plugin.PluginState;
import org.gcube.vremanagement.executor.plugin.PluginStateEvolution;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class SerializationTest {

	private static Logger logger = LoggerFactory.getLogger(SerializationTest.class);
		
	@Test
	public void testPluginEvolutionState() throws InvalidPluginStateEvolutionException, JsonParseException, JsonMappingException, IOException {
		PluginDefinition pluginDeclaration = new PluginDefinition() {
			
			@Override
			public String getGroup() {
				return "SmartExecutorGroup";
			}
			
			@Override
			public String getName() {
				return PluginDefinition.class.getSimpleName();
			}

			@Override
			public String getVersion() {
				return "1.0.0";
			}

			@Override
			public String getDescription() {
				return PluginDefinition.class.getSimpleName() + " Description";
			}
			
			@Override
			public Map<String, String> getSupportedCapabilities() {
				return new HashMap<String, String>();
			}

			@Override
			public String toString(){
				return String.format("%s :{ %s - %s - %s }", 
						PluginDefinition.class.getSimpleName(), 
						getName(), getVersion(), getDescription());
			}

		};
		
		
		PluginStateEvolution pes = new PluginStateEvolution(UUID.randomUUID(), 1, Calendar.getInstance().getTimeInMillis(), pluginDeclaration, PluginState.RUNNING, 10);
		logger.debug("{} to be Marshalled : {}", pes.getClass().getSimpleName(), pes);
		
		String marshalledPES = SEMapper.getInstance().marshal(pes);
		
		logger.debug("Marshalled {} : {}", PluginStateEvolution.class.getSimpleName(), marshalledPES);
		
		PluginStateEvolution pesUnmarshalled = SEMapper.getInstance().unmarshal(PluginStateEvolution.class, marshalledPES);
        logger.debug("UnMarshalled : {}", pesUnmarshalled);
	}
	
	
	@Test
	public void testUnmarshallPluginList() throws JsonParseException, JsonMappingException, IOException {
		String pluginList = "[{\"@class\":\"PluginDeclaration\",\"description\":\"Hello World Description\",\"supportedCapabilities\":{\"FakeKey\":\"FakeValue\"},\"version\":\"1.1.2\",\"name\":\"HelloWorld\"}]";
		List<PluginDefinition> list = SEMapper.getInstance().unmarshalList(PluginDefinition.class, pluginList);
		logger.debug("{}", SEMapper.getInstance().marshal(PluginDefinition.class, list));
	}
}
